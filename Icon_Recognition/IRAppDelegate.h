//
//  IRAppDelegate.h
//  Icon_Recognition
//
//  Created by bradypus on 13-1-27.
//  Copyright (c) 2013年 ece.cornell. All rights reserved.
//

#import <UIKit/UIKit.h>


@class IRViewController;

@interface IRAppDelegate : UIResponder <UIApplicationDelegate>{
    NSInteger globalPage;
    double glX;
    double glY;
    double glTopCenterX;
    double glTopCenterY;
    double glBottomCenterX;
    double glBottomCenterY;
    
    float glRotationX;
    float glRotationY;
    float glRotationZ;
    
    double globalAngle;
}

//@property (strong,nonatomic)  std::vector<ImageWithFeature*> imageDatabase;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) UINavigationController *navController;
@property(nonatomic) NSInteger globalPage;
@property (strong, nonatomic) IRViewController *viewController;
@property (nonatomic) double glX;
@property (nonatomic) double glY;
@property (nonatomic) float glRotationX;
@property (nonatomic) float glRotationY;
@property (nonatomic) float glRotationZ;
@property (nonatomic) double globalAngle;
@property (nonatomic) double glTopCenterX;
@property (nonatomic) double glTopCenterY;
@property (nonatomic) double glBottomCenterX;
@property (nonatomic) double glBottomCenterY;

@end
