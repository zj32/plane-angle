//
//  PageViewController.m
//  Icon_Recognition
//
//  Created by bradypus on 13-3-7.
//  Copyright (c) 2013年 ece.cornell. All rights reserved.
//

#import "PageViewController.h"
#import "IRViewController.h"
#import "IRAppDelegate.h"


@interface PageViewController ()

@property (nonatomic, strong) NSArray *pageImages;
@property (nonatomic, strong) NSMutableArray *pageViews;

- (void)loadVisiblePages;
- (void)loadPage:(NSInteger)page;
- (void)purgePage:(NSInteger)page;

@end

@implementation PageViewController
@synthesize pageImages;
@synthesize pageViews;

@synthesize scrollView;
@synthesize pageControl;

//@synthesize page;
@synthesize imageNames;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)loadVisiblePages {
    IRAppDelegate *appDelegate = (IRAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    appDelegate.globalPage = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    // Update the page control
    self.pageControl.currentPage = appDelegate.globalPage;
    
    // Work out which pages you want to load
    NSInteger firstPage = appDelegate.globalPage - 1;
    NSInteger lastPage = appDelegate.globalPage + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) {
        [self purgePage:i];
    }
    
	// Load pages in our range
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    
	// Purge anything after the last page
    for (NSInteger i=lastPage+1; i<self.pageImages.count; i++) {
        [self purgePage:i];
    }
}



- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
   
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {
        
        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
    
        UIImageView *newPageView = [[UIImageView alloc] initWithImage:[self.pageImages objectAtIndex:page]];
        newPageView.contentMode = UIViewContentModeScaleAspectFit;
        newPageView.frame = frame;
        [self.scrollView addSubview:newPageView];
       
        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
}

- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSMutableArray* arrPageImages = [[NSMutableArray alloc] init];
    int i;
    for (i = 0;i< imageNames.count;i++) {
        [arrPageImages addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
    }
    self.pageImages = [NSArray arrayWithArray:arrPageImages];
  
    
    NSInteger pageCount = self.pageImages.count;
    
    
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = pageCount;
    
    
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }
    
    CGFloat contentWidth = self.pageImages.count * SCROLL_VIEW_WIDTH;
    CGFloat contentHeight = SCROLL_VIEW_HEIGHT;
    self.scrollView.contentSize = CGSizeMake(contentWidth, contentHeight);
    [self loadVisiblePages];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Load the pages that are now on screen
    [self loadVisiblePages];
}

- (IBAction)useIt:(id)sender {
   
    IRViewController *irViewController = [[IRViewController alloc] initWithNibName:@"IRViewController" bundle:nil];
    irViewController.imageNames = self.imageNames;
    [self.navigationController pushViewController:irViewController animated:YES];
}


@end
