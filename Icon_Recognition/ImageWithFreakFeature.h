//
//  ImageWithFreakFeature.h
//  Icon_Recognition
//
//  Created by bradypus on 13-2-25.
//  Copyright (c) 2013年 ece.cornell. All rights reserved.
//
#import "ImageWithFeature.h"

const double FREAK_FEATURE_PARA_1 = 1000;
const double FREAK_FEATURE_PARA_2 = 4;
const double MINIMUM_FREAK_MATCH_DIS = 180;

@interface ImageWithFreakFeature : ImageWithFeature

@end
