//
//  ImageWithOrbFeature.m
//  Icon_Recognition
//
//  Created by bradypus on 13-2-25.
//  Copyright (c) 2013年 ece.cornell. All rights reserved.
//

#import "ImageWithOrbFeature.h"

@implementation ImageWithOrbFeature


-(id) computeFeature{
    cv::FeatureDetector* detector = new cv::OrbFeatureDetector();
    detector->detect(self.thisCVImage, self.featureLocation);
    
    cv::DescriptorExtractor* extractor = new cv::OrbDescriptorExtractor();
    cv::Mat descriptor;
    extractor->compute(self.thisCVImage, self.featureLocation, descriptor);
    self.featureDescriptor = descriptor;
    return 0;
}

-(void) setMatchValues {
    self.minMatchDistance = MIN_FEATURE_MATCH_DISTANCE_ORB;
    self.matcher = new cv::BFMatcher(cv::NORM_HAMMING);
}

@end
