//
//  ImageWithOrbFeature.h
//  Icon_Recognition
//
//  Created by bradypus on 13-2-25.
//  Copyright (c) 2013年 ece.cornell. All rights reserved.
//

#import "ImageWithFeature.h"
const int MIN_FEATURE_MATCH_DISTANCE_ORB = 30;
@interface ImageWithOrbFeature : ImageWithFeature

@end

