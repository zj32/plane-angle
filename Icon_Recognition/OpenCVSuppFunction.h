//
//  OpenCVSuppFunction.h
//  Icon_Recognition
//
//  Created by Zhaoyin Jia on 2/6/13.
//  Copyright (c) 2013 ece.cornell. All rights reserved.
//

#ifndef Icon_Recognition_OpenCVSuppFunction_h
#define Icon_Recognition_OpenCVSuppFunction_h

#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp> // for homography

// Drawing constant
const int RADIUS = 2;
const int THICKNESS_CIRCLE= 1;
const int THICKNESS_LINE= 1;

const cv::Scalar DRAW_COLOR= cv::Scalar(255,0,0);
const cv::Scalar DRAW_COLOR1= cv::Scalar(0,255,0);


using namespace std;
using namespace cv;

void correctSignOfH(Mat& H, Mat& K, vector<Point2f>& curLoc, vector<Point2f>& databaseLoc, Mat& Hout);

double getAngleFromH(Mat& H, Mat& g);

void drawHomographyBoundBox(Mat& databaseImg, vector<Point2f>& curLoc, vector<Point2f>& databaseLoc, Mat& imgToEdit, Mat& H, vector<Point2f>* avgCornersQueue);

void drawFeatureLoc(Mat& imgEdit, vector<cv::Point2f>& featPoints);

#endif


