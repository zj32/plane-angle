//
//  IRViewController.h
//  Icon_Recognition
//
//  Created by bradypus on 13-1-27.
//  Copyright (c) 2013年 ece.cornell. All rights reserved.
//
#include "OpenCVSuppFunction.h"
#include "ImageWithFeature.h"
#import <UIKit/UIKit.h>
#import <opencv2/highgui/cap_ios.h>
#import <CoreMotion/CoreMotion.h> // For motion sensor
#import <AssetsLibrary/AssetsLibrary.h>
using namespace cv;

const int VIDEO_INPUT_FRAME_RATE = 30;
const double IMAGE_TIMER_UPDATE_RATE = 0.02f;
const int ANG_QUEUE_LENGTH=12;
const int CORNERS_QUEUE_LENGTH=3;

@interface IRViewController : UIViewController<CvVideoCameraDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    CMMotionManager *motionManager;
    NSTimer *timer;
    NSTimer *timerImg;
    NSTimer *timerAngle;
    
    cv::Mat g;
    CvVideoCamera* videoCamera;
    NSString* labelText;
    
    double ang;
    double* angQueue;
    int angIndex;
    int angCount;
    double angAvg;
    
   
    
    UIButton * choosePhotoBtn;
	UIButton * takePhotoBtn;
}

@property (nonatomic,retain) CvVideoCamera* videoCamera;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *startButton;
@property (strong, nonatomic) IBOutlet UILabel *ImageName;
@property (strong, nonatomic) IBOutlet UILabel *lblOrientation;
@property (strong, nonatomic) IBOutlet UILabel *lblAngle;
@property (nonatomic) cv::Mat gravity;
@property (strong, nonatomic) IBOutlet UIButton *choosePhotoBtn;
@property (strong, nonatomic) IBOutlet UIButton *takePhotoBtn;
@property (strong, atomic) ALAssetsLibrary* library;
@property (atomic) cv::Mat cvImage;
@property (atomic) cv::vector<cv::Point2f>* averageCornersQueue;
@property (atomic) std::vector<ImageWithFeature*> imageDatabase;

@property(nonatomic) NSMutableArray* imageNames;


- (IBAction)toggleCamera:(id)sender;
- (IBAction) getPhoto:(id) sender;


@end
