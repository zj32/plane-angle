//
//  ImageWithFreakFeature.m
//  Icon_Recognition
//
//  Created by bradypus on 13-2-25.
//  Copyright (c) 2013年 ece.cornell. All rights reserved.
//

#import "ImageWithFeature.h"
#import "ImageWithFreakFeature.h"

@implementation ImageWithFreakFeature

-(id) computeFeature{
    // TODO: Add exception for empty image
    
    cv::SurfFeatureDetector detector(FREAK_FEATURE_PARA_1,FREAK_FEATURE_PARA_2);
    detector.detect(self.thisCVImage, self.featureLocation);
    
    cv::FREAK extractor;
    cv::Mat discriptor;
    extractor.compute(self.thisCVImage, self.featureLocation, discriptor);
    self.featureDescriptor = discriptor;
    return 0;
}

-(void) setMatchValues {
    self.minMatchDistance = MINIMUM_FREAK_MATCH_DIS;
    self.matcher = new cv::BFMatcher(cv::NORM_HAMMING);
}
@end
