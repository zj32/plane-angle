//
//  ImageWithFeature.m
//  Icon_Recognition
//
//  Created by Zhaoyin Jia on 2/6/13.
//  Copyright (c) 2013 ece.cornell. All rights reserved.
//

#import "ImageWithFeature.h"

@implementation ImageWithFeature

-(id) computeFeature{
    // TODO: Add exception for empty image
    // Override this function for other features
    
    cv::FeatureDetector* detector = new cv::SIFT();
    detector->detect(_thisCVImage, self.featureLocation);
    
    cv::DescriptorExtractor* extractor = new cv::SIFT();
    cv::Mat descriptor;
    extractor->compute(_thisCVImage, _featureLocation, descriptor);
    self.featureDescriptor = descriptor;
    return 0;
}

-(int) matchImg: (ImageWithFeature*) otherImage
                             locThis: (std::vector<cv::Point2f>&) thisMatchedLoc
                            locOther: (std::vector<cv::Point2f>&) otherMatchedLoc {
    
    // This function returns the score for SIFT matching
    // Override this function for other features
    // Simple matching algorithm following this website:
    // feature_flann_matcher.html
    
    if(otherImage.featureDescriptor.rows < 10) {
        return 0;
    }
    std::vector<cv::DMatch > matches;
    NSLog(@"database has %d features, target has %d features", self.featureDescriptor.rows,otherImage.featureDescriptor.rows);
    (self.matcher)->match(self.featureDescriptor, otherImage.featureDescriptor, matches);
    
    int i;
    std::vector<cv::DMatch> goodMatches;
    for(i = 0; i < self.featureDescriptor.rows; i++ ) {
        if( matches[i].distance < self.minMatchDistance) {
            goodMatches.push_back(matches[i]);
        }
    }
    NSLog(@"Matched %ld features", goodMatches.size());
    
    std::vector<cv::KeyPoint> otherImageKeyPointLoc = otherImage.featureLocation;
    int trainIdx, queryIdx;
    for(i=0;i < goodMatches.size(); i++) {
        trainIdx = goodMatches[i].trainIdx;
        queryIdx = goodMatches[i].queryIdx;
        
        thisMatchedLoc.push_back(_featureLocation[queryIdx].pt);
        otherMatchedLoc.push_back(otherImageKeyPointLoc[trainIdx].pt);
    }
    return goodMatches.size();
}

-(id) initWithCVMat: (cv::Mat&) cvMatInput{
    _thisUIImage = [UIImage imageWithCVMat:cvMatInput];
    _thisCVImage = cvMatInput;
    _imgName = @"No name";
    
    [self setMatchValues];
    // Extrac key points and features
    [self computeFeature];
    return self;
}

-(id) initWithName:(NSString*) imageName {
    _thisUIImage = [UIImage imageNamed:imageName];
    _thisCVImage = [_thisUIImage CVMat];
    _imgName = imageName;
    
    [self setMatchValues];
    // Extrac key points and features
    
    [self computeFeature];
    return self;
}

-(void) setMatchValues {
    self.minMatchDistance = MIN_FEATURE_MATCH_DISTANCE;
    self.matcher = new cv::BFMatcher(cv::NORM_L2);
}

-(void)dealloc {
    delete self.matcher;
}
@end
