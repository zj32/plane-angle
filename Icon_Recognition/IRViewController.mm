//
//  IRViewController.m
//  Icon_Recognition
//
//  Created by bradypus on 13-1-27.
//  Copyright (c) 2013年 ece.cornell. All rights reserved.
//

#import "UIImage+OpenCV.h"
#import "ImageWithFeature.h"
#import "ImageWithOrbFeature.h"
#import "ImageWithFreakFeature.h"
#import "IRViewController.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "IRAppDelegate.h"
#import "OpenGLView.h"

/*
// Name of face cascade resource file without xml extension
NSString * const kFaceCascadeFilename = @"haarcascade_frontalface_alt2";

// Options for cv::CascadeClassifier::detectMultiScale
const int kHaarOptions =  CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_DO_ROUGH_SEARCH;
*/

@interface IRViewController()
@end

@implementation IRViewController
@synthesize imageView;
@synthesize startButton;
@synthesize videoCamera;
@synthesize ImageName;
@synthesize lblOrientation;
@synthesize lblAngle;
@synthesize choosePhotoBtn, takePhotoBtn;
@synthesize library;
@synthesize cvImage;
@synthesize imageNames;
@synthesize averageCornersQueue;
@synthesize imageDatabase;


// This is the intrinsic paramter for iPhone
cv::Mat K =(cv::Mat_<double>(3,3, CV_64F) << 451.5, 0, 180.4, 0, 451.5,249.0,0,0,1 );

-(void) matchImgProcess:(NSTimer *)timer {
    // Get a copy of the orginal mat
    if(self.cvImage.empty()) {
        return;
    }
    Mat image_copy;
    cvtColor(self.cvImage, image_copy, CV_BGRA2BGR);
    
    ImageWithOrbFeature* curImg = [[ImageWithOrbFeature alloc] initWithCVMat:image_copy];
    
    std::vector<std::vector<cv::Point2f> > curLoc, databaseLoc;
    
    int i;
    int maxScore = 0, maxIndex = -1;
    for( i=0; i< imageDatabase.size(); i++) {
        std::vector<cv::Point2f> tmpCurLoc;
        std::vector<cv::Point2f> tmpDatabaseLoc;
        double curScore = [imageDatabase[i] matchImg:curImg locThis:tmpDatabaseLoc locOther:tmpCurLoc];
        curLoc.push_back(tmpCurLoc);
        databaseLoc.push_back(tmpDatabaseLoc);
        if (curScore > MIN_NUM_OF_MATCHED_FEATURE && curScore > maxScore) {
            maxIndex = i;
            maxScore = curScore;
        }
    }
    
    // Now we selected the bast match, get homography
    cv::Mat imgDatabase, imgEdit;
    
    imgEdit = curImg.thisCVImage;
    if (maxScore != 0) {
        cv::Mat H(3,3, CV_64F);
        cv::Mat Hn(3,3,CV_64F);
        labelText = imageDatabase[maxIndex].imgName;
        std::vector<cv::Point2f> finalCurLoc = curLoc[maxIndex];
        std::vector<cv::Point2f> finalDatabaseLoc = databaseLoc[maxIndex];
        imgDatabase = imageDatabase[maxIndex].thisCVImage;
        drawHomographyBoundBox(imgDatabase, finalCurLoc, finalDatabaseLoc, imgEdit, H,averageCornersQueue);
        correctSignOfH(H, K,  finalCurLoc,  finalDatabaseLoc, Hn);
        ang = getAngleFromH(Hn, g);
    } 
    
    //update the image view
    Mat outImage;
    cvtColor(imgEdit, outImage, CV_RGB2BGR);
    imageView.image = [UIImage imageWithCVMat:outImage];
}

- (void)updateMotionData:(NSTimer *)timer
{
    // Updating the gravity vector data 
    lblOrientation.text=@"";
    g.at<double>(0, 0) = motionManager.deviceMotion.gravity.x;
    lblOrientation.text=[lblOrientation.text stringByAppendingFormat:@"%2.2f ",
                  g.at<double>(0, 0)];
    g.at<double>(1, 0) = motionManager.deviceMotion.gravity.y;
    lblOrientation.text=[lblOrientation.text stringByAppendingFormat:@"%2.2f ",
                  g.at<double>(1,0)];
    g.at<double>(2, 0) = motionManager.deviceMotion.gravity.z;
    lblOrientation.text=[lblOrientation.text stringByAppendingFormat:@"%2.2f",
                  g.at<double>(2, 0)];
    
    //updating other motion data
    [self computeAngleAverage];
    lblAngle.text = [NSString stringWithFormat:@"%d°", (int)angAvg];
    
    ImageName.text = labelText;
    
    IRAppDelegate *appDelegate = (IRAppDelegate*) [[UIApplication sharedApplication] delegate];
    appDelegate.globalAngle=angAvg;
    
    NSLog(@"globalAngle: %f",appDelegate.globalAngle);
    
}

-(void) computeAngleAverage{
    
    angQueue[angIndex] = ang;
    angIndex = (angIndex+1)%ANG_QUEUE_LENGTH;
    angAvg = 0;
    int maxIndex;
    double maxAng = -1;
    int minIndex;
    double minAng = 91;
    if (angCount >= ANG_QUEUE_LENGTH) {
        //find the max/min angle in the queue
        for (int i = 0;i<ANG_QUEUE_LENGTH ; i++) {
            if (angQueue[i]>maxAng) {
                maxIndex = i;
                maxAng = angQueue[i];
            }
            if (angQueue[i]<minAng) {
                minIndex = i;
                minAng = angQueue[i];
            }
        }
        //exclude the max and min value while computing average
        for (int i = 0; i<ANG_QUEUE_LENGTH; i++) {
            if (i!=maxIndex && i!=minIndex) {
                angAvg += angQueue[i];
            }
            
        }
        
        angAvg /= (ANG_QUEUE_LENGTH-2);
    }
    
    else{
        angCount++;
        angAvg = ang;
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    angQueue = new double[ANG_QUEUE_LENGTH];
    angIndex = 0;
    angCount = 0;
    averageCornersQueue= new cv::vector<cv::Point2f>[CORNERS_QUEUE_LENGTH];
    
    self.library = [[ALAssetsLibrary alloc] init];
    IRAppDelegate *appDelegate = (IRAppDelegate*) [[UIApplication sharedApplication] delegate];
    
    imageDatabase.push_back([[ImageWithOrbFeature alloc] initWithName:imageNames[appDelegate.globalPage]]);
    
    //initialize and start the video
    [self initialCamera];
    
    // Motion stuff:
    motionManager = [[CMMotionManager alloc] init];
    motionManager.accelerometerUpdateInterval = 0.01;
    motionManager.deviceMotionUpdateInterval = 0.01;
    [motionManager startDeviceMotionUpdates];
   
    //initialize the motion data
    g = (Mat_<double>(3,1,CV_64F) << 0, 0, 1);
    ang = 0;
    labelText = @"No detection";
    

    //add the openGL view
//    EAGLContext *context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
//    [EAGLContext setCurrentContext:context];
//    OpenGLView *glView = [[OpenGLView alloc] initWithFrame:CGRectMake(0, 0, 480,960 )];
//    [self.view addSubview:glView];
}

- (void)viewDidUnload
{
    self.library = nil;
    [super viewDidUnload];
}


- (void)viewDidAppear:(BOOL)animated {
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.2f
                                             target:self
                                           selector:@selector(updateMotionData:)
                                           userInfo:nil
                                            repeats:YES];
    
    timerImg = [NSTimer scheduledTimerWithTimeInterval:IMAGE_TIMER_UPDATE_RATE
                                                target:self
                                              selector:@selector(matchImgProcess:)
                                              userInfo:nil
                                               repeats:YES];
    //add the openGL view
    EAGLContext *context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    [EAGLContext setCurrentContext:context];
    OpenGLView *glView = [[OpenGLView alloc] initWithFrame:CGRectMake(0, 0, 480,960 )];
    [self.view addSubview:glView];
}

-(void) viewDidDisappear:(BOOL)animated {
   
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any
    
    
    
//    resources that can be recreated.
}

- (void)initialCamera{
    //initialize the video
    //self.videoCamera = [[CvVideoCamera alloc] initWithParentView:imageView];
    self.videoCamera = [[CvVideoCamera alloc] init];
    self.videoCamera.delegate = self;
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset352x288;
    self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.videoCamera.defaultFPS = VIDEO_INPUT_FRAME_RATE;
    //start the camera
    [self.videoCamera start];
}

// MARK: IBActions
- (IBAction)toggleCamera:(id)sender {
    [self initialCamera];
}

-(IBAction) getPhoto:(id) sender {
	UIImagePickerController * picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
    
	if((UIButton *) sender == choosePhotoBtn) {
		picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
	} else {
		picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.editing = YES;
        picker.delegate = (id)self;
	}
    
	[self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo 
{
    cv::Mat newImageMat = [image CVMat];
    if(picker.sourceType == UIImagePickerControllerSourceTypeCamera){
        //save the photo to album
        [self.library saveImage:image toAlbum:@"Icon Recognition" withCompletionBlock:^(NSError *error) {
            if (error!=nil) {
                NSLog(@"Big error: %@", [error description]);
            }
        }];
        //resize the taken photo
        cv::resize(newImageMat, newImageMat, cv::Size(), 0.2f, 0.2f, CV_INTER_LINEAR);
    }
   
    //save the photo to database
    
    imageDatabase.push_back([[ImageWithOrbFeature alloc] initWithCVMat:newImageMat]);
    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    [self initialCamera];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    [self initialCamera];
}


// MARK: VideoCaptureViewController overrides

- (void)processImage:(cv::Mat&)mat;
{
    if(!mat.empty()) {
        mat.copyTo(cvImage);
    }
}

@end
