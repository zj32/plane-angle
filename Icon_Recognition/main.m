//
//  main.m
//  Icon_Recognition
//
//  Created by bradypus on 13-1-27.
//  Copyright (c) 2013年 ece.cornell. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IRAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IRAppDelegate class]));
    }
}
