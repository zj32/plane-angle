//
//  ImageWithFeature.h
//  Icon_Recognition
//
//  Created by Zhaoyin Jia on 2/6/13.
//  Copyright (c) 2013 ece.cornell. All rights reserved.
//


#import "UIImage+OpenCV.h"

#import <opencv2/highgui/cap_ios.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp> // for homography

const double MIN_FEATURE_MATCH_DISTANCE = 200;
const int MIN_NUM_OF_MATCHED_FEATURE = 5;
const double MAX_DISTANCE_ALLOWED_BY_MIN_DIS = 3;

@interface ImageWithFeature : NSObject

-(void) setMatchValues;
-(id) initWithName:(NSString*) imageName;
-(id) initWithCVMat: (cv::Mat&) cvMatInput;
-(id) computeFeature;
-(int) matchImg: (ImageWithFeature*) otherImage
locThis: (std::vector<cv::Point2f>&) thisMatchedLoc
locOther: (std::vector<cv::Point2f>&) otherMatchedLoc;

-(cv::Mat) drawFeatureLoc: (std::vector<cv::Point2f>&) featPoints;

@property(nonatomic) std::vector<cv::KeyPoint>& featureLocation;
@property cv::Mat featureDescriptor;
@property(nonatomic) UIImage* thisUIImage;
@property(nonatomic) cv::Mat thisCVImage;
@property(nonatomic) NSString* imgName;
@property(nonatomic) double minMatchDistance;
@property(nonatomic) cv::BFMatcher* matcher;

@end
