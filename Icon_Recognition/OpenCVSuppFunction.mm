//
//  OpenCVSuppFunction.mm
//  Icon_Recognition
//
//  Created by Zhaoyin Jia on 2/6/13.
//  Copyright (c) 2013 ece.cornell. All rights reserved.
//

#include "OpenCVSuppFunction.h"
#include "IRViewController.h"
#include "IRAppDelegate.h"

int queueCount = 0;
int queueIndex = 0;
double getAngleFromH(Mat& Hn, Mat& g) {
    Mat v1 = (Mat_<double>(3,1,CV_64F) << 1, 0, 0);
    Mat u1 = (Mat_<double>(3,1,CV_64F) << 0, 1, 0);
    
    Mat h1 = Hn*v1;
    Mat h2 = Hn*u1;
    Mat h3 = h1.cross(h2);
    
    Mat R(3, 3, CV_64F);
    Mat R0 = R.col(0);
    Mat R1 = R.col(1);
    Mat R2 = R.col(2);
    
    h1.copyTo(R0);
    h2.copyTo(R1);
    h3.copyTo(R2);
        
    SVD R_svd(R);
    R = R_svd.u * R_svd.vt;
    g.at<double>(1, 0) = - g.at<double>(1, 0);
    g.at<double>(2, 0) = - g.at<double>(2, 0);
    g = g/norm(g);
    
    Mat n = (Mat_<double> (3, 1, CV_64F) << 0, 0, 1);
    n = R * n;
    n = n/norm(n);
    double ang = acos(g.dot(n));
    ang = ang/3.14159 * 180;
    if ( ang > 90) {
        ang = 180 - ang;
    }
    
//    NSLog(@"n: %f,%f,%f",n.at<double>(1,0))
    
    //get the rotation degrees for OpenGL
    IRAppDelegate *appDelegate = (IRAppDelegate*) [[UIApplication sharedApplication] delegate];
    appDelegate.glRotationX = asin(n.at<double>(0,0))/3.14159*180;
    appDelegate.glRotationY = acos(n.at<double>(1,0))/3.14159*180;
    appDelegate.glRotationZ = acos(n.at<double>(2,0))/3.14159*180;
    
    return ang;
}

void correctSignOfH(Mat& H, Mat& K, vector<Point2f>& curLoc, vector<Point2f>& databaseLoc, Mat& Hout) {
    // H*databaseloc =  curLoc;
    // First, normalize H by SVD
    Hout = K.inv()*H;
    
    SVD H_svd(Hout);
    double scalar = H_svd.w.at<double>(1,1);
    Hout = Hout / scalar;
    int i;
    int negPoints = 0;
    Mat curSign;
    for (i = 0; i< curLoc.size(); i++) {
        Mat xc = (Mat_<double>(3,1,CV_64F) << curLoc[i].x , curLoc[i].y, 1);
        Mat xd = (Mat_<double>(3,1,CV_64F) << databaseLoc[i].x , databaseLoc[i].y, 1);        
        vector<Point2f> dpoint,cpoint;
        dpoint.push_back(databaseLoc[i]);
        curSign = xc.t() * H  * xd;
        if (curSign.at<double>(0,0) < 0) {
            negPoints ++ ;
        }
        if( negPoints > 4) {
            // Got enough poitns that is smaller than 0
            Hout = -Hout;
            return;
        }
    }
}

bool isConvex(vector<Point2f> Corners){
    
    if (Corners[0].x < Corners[1].x
        && Corners[0].y < Corners[3].y
        && Corners[1].y < Corners[2].y
        && Corners[2].x > Corners[3].x) {
        return true;
    }
    
    return  false;
}

vector<cv::Point2f> computeAverageCorners(vector<Point2f>* avgCornersQueue,vector<Point2f> sceneCorners){
    vector<cv::Point2f> averageCorners;
    cv::Point2f p;
    p.x = 0;
    p.y = 0;
    
    if (averageCorners.empty()) {
        queueCount = 0;
        queueIndex = 0;
    }
    
    for (int i = 0; i<4; i++) {
        averageCorners.push_back(p);
    }
    if (isConvex(sceneCorners)) {
        avgCornersQueue[queueIndex] = sceneCorners;
        queueIndex = (queueIndex+1)%CORNERS_QUEUE_LENGTH;
        
        if (queueCount<CORNERS_QUEUE_LENGTH) {
            queueCount++;
            averageCorners = sceneCorners;
        }
    }
    
    
    
    if (queueCount>=CORNERS_QUEUE_LENGTH) {
        for (int i = 0; i<CORNERS_QUEUE_LENGTH; i++) {
            for (int j=0; j<4; j++) {
                averageCorners[j].x += avgCornersQueue[i][j].x;
                averageCorners[j].y += avgCornersQueue[i][j].y;
            }
        }
        for (int j = 0; j<4; j++) {
            averageCorners[j].x /= CORNERS_QUEUE_LENGTH;
            averageCorners[j].y /= CORNERS_QUEUE_LENGTH;
        }
    }
    
    return averageCorners;
}

void drawHomographyBoundBox(Mat& databaseImg, vector<Point2f>& curLoc, vector<Point2f>& databaseLoc, Mat& imgToEdit, Mat& H, vector<Point2f>* avgCornersQueue) {
    
    IRAppDelegate *appDelegate = (IRAppDelegate*) [[UIApplication sharedApplication] delegate];
    appDelegate.glX = 0;
    appDelegate.glY = 0;
    appDelegate.glTopCenterX = 0;
    appDelegate.glTopCenterY = 0;
    appDelegate.glBottomCenterX = 0;
    appDelegate.glBottomCenterY = 0;
    
    vector<cv::Point2f> objectCorners, sceneCorners;
    objectCorners.push_back(cvPoint(0,0));
    objectCorners.push_back(cvPoint(databaseImg.cols,0));
    objectCorners.push_back(cvPoint(databaseImg.cols,databaseImg.rows));
    objectCorners.push_back(cvPoint(0,databaseImg.rows));

    // Find homography
    H = cv::findHomography(databaseLoc, curLoc, cv::RANSAC);
    
    cv::perspectiveTransform(objectCorners, sceneCorners, H);
    // Draw lines between the corners (the mapped object in the scene image )
    sceneCorners = computeAverageCorners(avgCornersQueue,sceneCorners);
    for (int i = 0; i<4; i++) {
        cv::line(imgToEdit, sceneCorners[i], sceneCorners[(i+1)%4], DRAW_COLOR, THICKNESS_LINE );
        appDelegate.glX += sceneCorners[i].x;
        appDelegate.glY += sceneCorners[i].y;
        if (i < 2) {
            appDelegate.glTopCenterX += sceneCorners[i].x;
            appDelegate.glTopCenterY += sceneCorners[i].y;
        }
        else{
            appDelegate.glBottomCenterX += sceneCorners[i].x;
            appDelegate.glBottomCenterY += sceneCorners[i].y;
        }
    }

    //pass the coordinates value to the global variable.
    
    //get the coordinates of the center.
    appDelegate.glX /= 4;
    appDelegate.glY /= 4;

    appDelegate.glTopCenterX /= 2;
    appDelegate.glTopCenterY /= 2;
    appDelegate.glBottomCenterX /= 2;
    appDelegate.glBottomCenterY /= 2;

//    cv::Point2f p1;
//    p1.x = appDelegate.glX;
//    p1.y = appDelegate.glY;
//    
//    cv::circle(imgToEdit,p1,RADIUS,DRAW_COLOR1,THICKNESS_CIRCLE*3);

    NSLog(@"botomY",appDelegate.glBottomCenterY);

    appDelegate.glX = appDelegate.glX /288 *3.5 - 1.75;
    appDelegate.glY = 1.5 - appDelegate.glY/352*5;
    appDelegate.glTopCenterX = appDelegate.glTopCenterX/288*3.5 - 1.75;
    appDelegate.glBottomCenterX = appDelegate.glTopCenterX/288*3.5 - 1.75;
    appDelegate.glTopCenterY = 1.5 - appDelegate.glTopCenterY/352*5;
    appDelegate.glBottomCenterY = 1.5 - appDelegate.glBottomCenterY/352*5;
    
    
    NSLog(@"x:%f,y:%f",appDelegate.glX,appDelegate.glY);
    NSLog(@"topY:%f,botY:%f",appDelegate.glTopCenterY,appDelegate.glBottomCenterY);
}


void drawFeatureLoc(Mat& imgEdit, vector<cv::Point2f>& featPoints) {
    int i;
    for(i = 0; i< featPoints.size(); i++) {
        cv::circle(imgEdit,featPoints[i], RADIUS,DRAW_COLOR,THICKNESS_CIRCLE);
    }
}
