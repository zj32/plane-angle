//
//  PageViewController.h
//  Icon_Recognition
//
//  Created by bradypus on 13-3-7.
//  Copyright (c) 2013年 ece.cornell. All rights reserved.
//

#import <UIKit/UIKit.h>
#define SCROLL_VIEW_HEIGHT 264;
#define SCROLL_VIEW_WIDTH 360;

@interface PageViewController : UIViewController<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) NSMutableArray* imageNames;

- (IBAction)useIt:(id)sender;

@end
